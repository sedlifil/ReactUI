import React, {Component} from 'react';
import {Switch, Route} from 'react-router-dom'
import Home from "./Home";
import AuthorRouter from "./author/AuthorRouter";
import AuthorForm from "./author/CreateAuthor";
import LibraryRouter from "./library/LibraryRouter";
import LibraryForm from "./library/LibraryForm";
import PublisherRouter from "./publisher/PublisherRouter";
import PublisherForm from "./publisher/PublisherForm";
import BookRouter from "./book/BookRouter";
import BookForm from "./book/BookForm";
import AuthorContractPublisher from "./authorContractPublisher/AuthorContractPublisher";
import ContractConcluded from "./authorContractPublisher/ContractConcluded";
import PublisherPutOutBook from "./publisherPutOutBook/PublisherPutOutBook";
import BookWasPutOut from "./publisherPutOutBook/BookWasPutOut";
import BookToLibrary from "./bookToLibrary/BookToLibrary";
import BookAddedToLibrary from "./bookToLibrary/BookAddedToLibrary";


class MainRouter extends Component {


    render() {
        return (
            <main>
                <Switch>
                    <Route exact path='/' component={Home}/>
                    <Route path='/authors' component={AuthorRouter}/>
                    <Route exact path='/author' component={AuthorForm}/>

                    <Route path='/libraries' component={LibraryRouter}/>
                    <Route exact path='/library' component={LibraryForm}/>

                    <Route path='/publishers' component={PublisherRouter}/>
                    <Route exact path='/publisher' component={PublisherForm}/>

                    <Route path='/books' component={BookRouter}/>
                    <Route exact path='/book' component={BookForm}/>

                    <Route exact path='/authorsContractPublishers' component={AuthorContractPublisher}/>
                    <Route exact path='/contractConcluded' component={ContractConcluded}/>

                    <Route exact path='/publisherBookRegister' component={PublisherPutOutBook}/>
                    <Route exact path='/bookWasPutOut' component={BookWasPutOut}/>

                    <Route exact path='/bookToLibrary' component={BookToLibrary}/>
                    <Route exact path='/bookAddedToLibrary' component={BookAddedToLibrary}/>

                </Switch>
            </main>
        );
    }
}

export default MainRouter;