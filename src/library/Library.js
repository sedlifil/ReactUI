import React, {Component} from 'react';
import {Link} from 'react-router-dom';


import '../css/main.css';


class Library extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: props.match.params.id,
            error: null,
            isLoaded: false,
            library: {}
        };
    }


    componentDidMount() {
        fetch("http://localhost:8080/BookSystem/rest/libraries/" + this.state.id)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        library: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    static showAllBooks(author) {
        if (author.books.toString() !== "") {
            return (
                <ul>
                    {author.books.map(book => (
                        <li key={book.id}>
                            <Link to={`/books/${book.id}`}>{book.name}</Link>
                        </li>

                    ))}
                </ul>);
        } else {
            return (
                <ul>
                    <li>žádná kniha v knihovně</li>
                </ul>
            );
        }
    }

    goBack = (props) => {
        this.props.history.goBack();
    };

    render() {
        const {id, error, isLoaded, library} = this.state;

        if (error) {
            return <div>{id} Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else if (library) {
            return (
                <section>
                    <h1>{library.name}</h1>
                    <table>

                        <tr>
                            <th>
                                Adresa:
                            </th>
                            <td>
                                {library.address}
                            </td>
                        </tr>
                        <tr>

                        </tr>
                        <tr className=''>
                            <td>
                                Knihy v knihovně:<br/>
                                {Library.showAllBooks(library)}
                            </td>
                        </tr>
                        <tr>
                            <button onClick={this.goBack.bind(this)}>Zpátky</button>
                        </tr>
                    </table>


                </section>
            );
        }
    }
}

export default Library;