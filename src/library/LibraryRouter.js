import React, {Component} from 'react';
import {Switch, Route} from 'react-router-dom'
import Library from "./Library";
import Libraries from "./Libraries";

import '../css/main.css';

class LibraryRouter extends Component {

    render() {
        return (
            <Switch>
                <Route exact path='/libraries' component={Libraries}/>
                <Route path='/libraries/:id' component={Library}/>
            </Switch>
        );
    }
}

export default LibraryRouter;