import React, {Component} from 'react';
import {Link} from 'react-router-dom';


import '../css/main.css';


class Libraries extends Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            data: []
        };
    }


    componentDidMount() {
        fetch("http://localhost:8080/BookSystem/rest/libraries")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        data: result
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    showAllLibraries() {
        const {data} = this.state;
        return (
            <section>
                <h1>Knihovny</h1>
                <table className='cyanTemplate'>
                    <thead>
                    <tr>
                        <th>
                            Název
                        </th>
                        <th>
                            Adresa
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    {data.map(library => (
                        <tr
                            key={library.id}>
                            <td>
                                <Link to={`/libraries/${library.id}`}>{library.name}</Link>
                            </td>

                            <td>
                                {library.address}
                            </td>
                        </tr>
                    ))}
                    </tbody>
                </table>
            </section>
        );
    }


    render() {
        const {error, isLoaded, data} = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else if (data) {
            return this.showAllLibraries();
        }
    }
}

export default Libraries;