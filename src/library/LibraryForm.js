import React, {Component} from 'react';
import {Redirect} from 'react-router'
import moment from 'moment';

import '../css/main.css';
import '../css/form.css';
import AuthorForm from "../author/CreateAuthor";


class LibraryForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fireRedirect: false,
            name: "",
            address: "",
            errors: {
                name: false,
                address: false,
                added: false,
                serverError: ""
            }
        };
        this.handleName = this.handleName.bind(this);
        this.handleAddress = this.handleAddress.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleName() {

        const nameLibrary = document.getElementById('name').value;
        this.setState({name: nameLibrary});
        let errors = Object.assign({}, this.state.errors);    //creating copy of object
        if (nameLibrary.length <= 0) {
            errors.name = true;
            this.setState({errors});
            return false;
        }
        errors.name = false;
        this.setState({errors});
        return true;
    }

    handleAddress() {
        const address = document.getElementById('address').value;
        this.setState({address: address});
        let errors = Object.assign({}, this.state.errors);    //creating copy of object
        if (address.length <= 0) {
            errors.address = true;
            this.setState({errors});
            return false;
        }
        errors.address = false;
        this.setState({errors});
        return true;
    }

    handleSubmit(e) {
        e.preventDefault();
        if (!this.handleName()) {
            return;
        }
        if (!this.handleAddress()) {
            return;
        }

        const library = {"name": this.state.name, "address": this.state.address};
        fetch('http://localhost:8080/BookSystem/rest/libraries', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(library)
        }).then(function (response) {
            if (!response) {
                throw Error(response.statusText);
            }
            return response;
        }).then((response) => {
            if (response.status === 200) {
                this.setState({fireRedirect: true});
                console.log("ok");
            } else if (response.status === 512) {
                let errors = Object.assign({}, this.state.errors);
                errors.added = true;
                errors.serverError = 'Error s vytvářením knihovny.';
                this.setState({errors});
                console.log(response.statusText);
            } else {
                console.log(response.statusText);
            }


        });
    }

    render() {
        if (this.state.fireRedirect) {
            return (
                <Redirect to={'/libraries'}/>
            );
        }
        return (

            <section>
                <h1>Vytvoř knihovnu</h1>
                <form className="ContactForm" onSubmit={this.handleSubmit}>
                    <label htmlFor={'name'}>Název:</label>
                    <input id={'name'} className={AuthorForm.contactFormErrorClass(this.state.errors.name)}
                           type={'text'} value={this.state.name} placeholder={'název'} onChange={this.handleName}/>
                    <label className={'error' + ' ' + AuthorForm.labelErrorClass(this.state.errors.name)}>Prosím vyplňte
                        název.</label>

                    <label htmlFor={'address'}>Adresa:</label>
                    <input id={'address'} className={AuthorForm.contactFormErrorClass(this.state.errors.address)}
                           type={'text'} value={this.state.address} placeholder={'adresa'}
                           onChange={this.handleAddress}/>
                    <label className={'error' + ' ' + AuthorForm.labelErrorClass(this.state.errors.address)}>Prosím
                        vyplňte adresu.</label>

                    <input type={'submit'} value={'Vytvoř knihovnu'}/>
                    <p className={'error' + ' ' + AuthorForm.labelErrorClass(this.state.errors.added)}>
                        {this.state.errors.serverError}
                    </p>
                </form>
            </section>


        );
    }

}

export default LibraryForm;






