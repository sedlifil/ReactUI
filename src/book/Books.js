import React, {Component} from 'react';
import {Link} from 'react-router-dom';


import '../css/main.css';


class Books extends Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            data: []
        };
    }


    componentDidMount() {
        fetch("http://localhost:8080/BookSystem/rest/books")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        data: result
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    showAllBooks() {
        const {data} = this.state;
        return (
            <section>
                <h1>Knihy</h1>
                <table className='cyanTemplate'>
                    <thead>
                    <tr>

                        <th>
                            ISBN
                        </th>
                        <th>
                            Název
                        </th>
                        <th>
                            Titul
                        </th>
                        <th>
                            Datum vydání
                        </th>
                        <th>
                            Autor
                        </th>
                        <th>
                            Nakladatelství
                        </th>

                    </tr>
                    </thead>
                    <tbody>
                    {data.map(book => (
                        <tr
                            key={book.id}>
                            <td>
                                {book.isbn}
                            </td>
                            <td>
                                <Link to={`/books/${book.id}`}>{book.name}</Link>
                            </td>
                            <td>
                                {book.title}
                            </td>
                            <td>
                                {book.publishDate}
                            </td>
                            <td>
                                {book.publisher.name}
                            </td>
                            <td>
                                {book.author.name} {book.author.surname}
                            </td>
                        </tr>
                    ))}
                    </tbody>
                </table>
            </section>
        );
    }


    render() {
        const {error, isLoaded, data} = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else if (data) {
            return this.showAllBooks();
        }
    }
}

export default Books;