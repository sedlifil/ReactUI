import React, {Component} from 'react';
import {Link} from 'react-router-dom';


import '../css/main.css';


class Book extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: props.match.params.id,
            error: null,
            isLoaded: false,
            book: {}
        };
    }


    componentDidMount() {
        fetch("http://localhost:8080/BookSystem/rest/books/" + this.state.id)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        book: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    showAllLibraries() {
        const {book} = this.state;
        if (book.libraries.toString() !== "") {
            return (
                <ul>
                    {book.libraries.map(library => (
                        <li key={library.id}>
                            <Link to={`/libraries/${library.id}`}>{library.name}</Link>
                        </li>

                    ))}
                </ul>);
        } else {
            return (
                <ul>
                    <li>kniha není v žádné knihovně</li>
                </ul>
            );
        }
    }

    goBack = (props) => {
        this.props.history.goBack();
    };

    render() {
        const {id, error, isLoaded, book} = this.state;

        if (error) {
            return <div>{id} Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else if (book) {
            return (
                <section>
                    <h1>{book.name}</h1>
                    <table>

                        <tr>
                            <th>
                                ISBN:
                            </th>
                            <td>
                                {book.isbn}
                            </td>

                        </tr>

                        <tr>
                            <th>
                                Žánr:
                            </th>
                            <td>
                                {book.title}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Datum vydání:
                            </th>
                            <td>
                                {book.publishDate}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Nakladatelství:
                            </th>
                            <td>
                                <Link to={`/publishers/${book.publisher.id}`}>{book.publisher.name}</Link>

                            </td>
                        </tr>

                        <tr>
                            <th>
                                Autor:
                            </th>
                            <td>
                                <Link to={`/authors/${book.author.id}`}>{book.author.name} {book.author.surname}</Link>
                            </td>
                        </tr>

                        <tr className=''>
                            Kniha v knihovnách:<br/>
                            {this.showAllLibraries()}
                        </tr>

                        <tr>
                            <button onClick={this.goBack.bind(this)}>Zpátky</button>
                        </tr>
                    </table>


                </section>
            );
        }
    }
}

export default Book;