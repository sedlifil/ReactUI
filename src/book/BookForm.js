import React, {Component} from 'react';
import {Redirect} from 'react-router'
import moment from 'moment';

import '../css/main.css';
import '../css/form.css';
import AuthorForm from "../author/CreateAuthor";


class BookForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fireRedirect: false,
            name: "",
            isbn: "",
            title: "",
            publishDate: "",
            authorId: 0,
            publisherId: 0,
            publishers: [],
            authors: [],
            errors: {
                name: false,
                isbn: false,
                title: false,
                publishDate: false,
                authorId: false,
                publisherId: false,
                added: false,
                serverError: ""
            }
        };
        this.handleName = this.handleName.bind(this);
        this.handleIsbn = this.handleIsbn.bind(this);
        this.handleTitle = this.handleTitle.bind(this);
        this.handlePublishDate = this.handlePublishDate.bind(this);
        this.handlePublishDateBlur = this.handlePublishDateBlur.bind(this);
        this.handleChangePublisher = this.handleChangePublisher.bind(this);
        this.handleChangeAuthor = this.handleChangeAuthor.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        fetch("http://localhost:8080/BookSystem/rest/authors")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        authors: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
        fetch("http://localhost:8080/BookSystem/rest/publishers")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        publishers: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );

    }


    handleName() {
        const nameAuthor = document.getElementById('name').value;
        this.setState({name: nameAuthor});
        let errors = Object.assign({}, this.state.errors);
        if (nameAuthor.length <= 0) {
            errors.name = true;
            this.setState({errors});
            return false;
        }
        errors.name = false;
        this.setState({errors});
        return true;
    }

    handleIsbn() {
        const isbn = document.getElementById('isbn').value;
        this.setState({isbn: isbn});
        let errors = Object.assign({}, this.state.errors);
        if (isbn.length <= 0) {
            errors.isbn = true;
            this.setState({errors});
            return false;
        }
        errors.isbn = false;
        this.setState({errors});
        return true;
    }

    handleTitle() {
        const title = document.getElementById('title').value;
        this.setState({title: title});
        let errors = Object.assign({}, this.state.errors);
        if (title.length <= 0) {
            errors.title = true;
            this.setState({errors});
            return false;
        }
        errors.title = false;
        this.setState({errors});
        return true;
    }

    handlePublishDate() {
        const publishDate = document.getElementById('publishDate').value;
        this.setState({publishDate: publishDate});
        if (this.state.errors.publishDate) {
            this.handlePublishDateBlur();
        }
    }

    handlePublishDateBlur() {
        const publishDate = document.getElementById('publishDate').value;
        let errors = Object.assign({}, this.state.errors);
        if (moment(publishDate.replace(/-/g, '.'), 'D.M.YYYY', true).isValid()) {
            errors.publishDate = false;
            this.setState({errors});
            return true;
        }
        errors.publishDate = true;
        this.setState({errors});
        return false;
    }

    handleChangeAuthor() {
        const authorId = document.getElementById("author").value;
        this.setState({authorId: authorId});
        let errors = Object.assign({}, this.state.errors);
        if (parseInt(authorId) !== 0) {
            errors.authorId = false;
            this.setState({errors});
            return true;
        }
        errors.authorId = true;
        this.setState({errors});
        return false;
    }

    handleChangePublisher() {
        const publisherId = document.getElementById("publisher").value;
        this.setState({publisherId: publisherId});
        let errors = Object.assign({}, this.state.errors);
        if (parseInt(publisherId) !== 0) {
            errors.publisherId = false;
            this.setState({errors});
            return true;
        }
        errors.publisherId = true;
        this.setState({errors});
        return false;
    }


    handleSubmit(e) {
        e.preventDefault();
        if (!this.handleIsbn()) {
            return;
        }
        if (!this.handleName()) {
            return;
        }
        if (!this.handleTitle()) {
            return;
        }
        if (!this.handlePublishDateBlur()) {
            return;
        }
        if (!this.handleChangePublisher()) {
            return;
        }
        if (!this.handleChangeAuthor()) {
            return;
        }

        const book = {
            "name": this.state.name,
            "isbn": this.state.isbn,
            "title": this.state.title,
            "publishDate": this.state.publishDate
        };
        fetch('http://localhost:8080/BookSystem/rest/books/register/' + this.state.authorId + '/' + this.state.publisherId, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(book)
        }).then(function (response) {
            if (!response) {
                throw Error(response.statusText);
            }
            return response;
        }).then((response) => {
            if (response.status === 200) {
                this.setState({fireRedirect: true});
                console.log("ok");
            } else if (response.status === 511) {
                let errors = Object.assign({}, this.state.errors);
                errors.added = true;
                errors.serverError = 'Error s ukládáním knihy.';
                this.setState({errors});
                console.log(response.statusText);
            } else if (response.status === 512) {
                let errors = Object.assign({}, this.state.errors);
                errors.added = true;
                errors.serverError = 'Kniha nelze být vytvořena.';
                this.setState({errors});
                console.log(response.statusText);
            } else if (response.status === 513) {
                let errors = Object.assign({}, this.state.errors);
                errors.added = true;
                errors.serverError = 'ISBN z knihy je už používáno.';
                this.setState({errors});
                console.log(response.statusText);
            } else if (response.status === 514) {
                let errors = Object.assign({}, this.state.errors);
                errors.added = true;
                errors.serverError = 'Kniha musí mít autora a nakladatelství.';
                this.setState({errors});
                console.log(response.statusText);
            } else {
                console.log(response.statusText);
            }


        });
    }

    showAllAuthors() {

        return (
            <select value={this.state.authorId} name="author" id="author" onChange={this.handleChangeAuthor}
                    className={"form " + AuthorForm.contactFormErrorClass(this.state.errors.authorId)}>

                <option value="0" disabled="disabled">-- vyberte autora --</option>
                {this.state.authors.map(author => (
                    <option key={author.id} value={author.id}>
                        {author.name} {author.surname}, {author.dateOfBirth}
                    </option>
                ))}

            </select>
        );
    }

    showAllPublishers() {
        return (
            <select value={this.state.publisherId} name="publisher" id="publisher" onChange={this.handleChangePublisher}
                    className={"form " + AuthorForm.contactFormErrorClass(this.state.errors.publisherId)}>

                <option value="0" disabled="disabled">-- vyberte nakladatelství --</option>
                {this.state.publishers.map(publisher => (
                    <option key={publisher.id} value={publisher.id}>
                        {publisher.name}
                    </option>
                ))}

            </select>
        );
    }

    render() {
        if (this.state.fireRedirect) {
            return (
                <Redirect to={'/books'}/>
            );
        }
        return (

            <section>
                <h1>Vytvoř knihu</h1>
                <form className="ContactForm" onSubmit={this.handleSubmit}>
                    <label htmlFor={'isbn'}>ISBN:</label>
                    <input id={'isbn'} className={AuthorForm.contactFormErrorClass(this.state.errors.isbn)}
                           type={'text'}
                           value={this.state.isbn} placeholder={'ISBN'} onChange={this.handleIsbn}/>
                    <label className={'error' + ' ' + AuthorForm.labelErrorClass(this.state.errors.isbn)}>Prosím vyplňte
                        správně ISBN.</label>

                    <label htmlFor={'name'}>Název:</label>
                    <input id={'name'} className={AuthorForm.contactFormErrorClass(this.state.errors.name)}
                           type={'text'}
                           value={this.state.name} placeholder={'jméno'} onChange={this.handleName}/>
                    <label className={'error' + ' ' + AuthorForm.labelErrorClass(this.state.errors.name)}>Prosím vyplňte
                        jméno.</label>

                    <label htmlFor={'title'}>Titulek:</label>
                    <input id={'title'} className={AuthorForm.contactFormErrorClass(this.state.errors.title)}
                           type={'text'}
                           value={this.state.title}
                           placeholder={'titulek'} onChange={this.handleTitle}/>
                    <label className={'error' + ' ' + AuthorForm.labelErrorClass(this.state.errors.title)}>Prosím
                        vyplňte
                        titulek.</label>

                    <label htmlFor={'publishDate'}>Datum vydání:</label>
                    <input id={'publishDate'}
                           className={AuthorForm.contactFormErrorClass(this.state.errors.publishDate)}
                           type={'text'} value={this.state.publishDate}
                           placeholder={'datum vydání'} onChange={this.handlePublishDate}
                           onBlur={this.handlePublishDateBlur}/>
                    <label className={'error' + ' ' + AuthorForm.labelErrorClass(this.state.errors.publishDate)}>Prosím
                        vyplňte správně datum vydání.</label>
                    <label>Vyberte nakladatelství:</label>
                    {this.showAllPublishers()}
                    <label className={"error"}>.</label>
                    <label>Vyberte autora:</label>
                    {this.showAllAuthors()}
                    <label className={"error"}>.</label>
                    <input type={'submit'} value={'Vytvoř knihu'}/>
                    <p className={'error' + ' ' + AuthorForm.labelErrorClass(this.state.errors.added)}>
                        {this.state.errors.serverError}
                    </p>
                </form>
            </section>


        );
    }

}

export default BookForm;





