import React, {Component} from 'react';
import {Switch, Route} from 'react-router-dom'
import Book from "./Book";
import Books from "./Books";

import '../css/main.css';

class BookRouter extends Component {

    render() {
        return (
            <Switch>
                <Route exact path='/books' component={Books}/>
                <Route path='/books/:id' component={Book}/>
            </Switch>
        );
    }
}

export default BookRouter;