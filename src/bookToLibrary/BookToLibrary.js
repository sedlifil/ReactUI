import React, {Component} from 'react';
import {Redirect} from 'react-router'

import '../css/main.css';
import '../css/form.css';
import AuthorForm from "../author/CreateAuthor";


class BookToLibrary extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fireRedirect: false,
            books: [],
            bookId: 0,
            libraries: [],
            libraryId: 0,
            errors: {
                bookId: false,
                libraryId: false,
                added: false
            }
        };
        this.handleChangeBook = this.handleChangeBook.bind(this);
        this.handleChangeLibrary = this.handleChangeLibrary.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        fetch("http://localhost:8080/BookSystem/rest/books")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        books: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
        fetch("http://localhost:8080/BookSystem/rest/libraries")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        libraries: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );

    }

    handleChangeBook() {
        var bookId = document.getElementById("book").value;
        this.setState({bookId: bookId});
        let errors = Object.assign({}, this.state.errors);
        if (parseInt(bookId) !== 0) {
            errors.bookId = false;
            this.setState({errors});
            return true;
        }
        errors.bookId = true;
        this.setState({errors});
        return false;
    }

    handleChangeLibrary() {
        const libraryId = document.getElementById("library").value;
        this.setState({libraryId: libraryId});
        let errors = Object.assign({}, this.state.errors);
        if (parseInt(libraryId) !== 0) {
            errors.libraryId = false;
            this.setState({errors});
            return true;
        }
        errors.libraryId = true;
        this.setState({errors});
        return false;
    }


    handleSubmit(e) {
        e.preventDefault();
        if (!this.handleChangeLibrary()) {
            return;
        }
        if (!this.handleChangeBook()) {
            return;
        }

        const json = {"idBook": this.state.bookId, "idLibrary": this.state.libraryId};

        fetch('http://localhost:8080/BookSystem/rest/bookToLibrary', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(json)
        }).then(function (response) {
            if (!response) {
                throw Error(response.statusText);
            }
            return response;
        }).then((response) => {
            if (response.status === 200) {
                this.setState({fireRedirect: true});
                console.log("ok");
            } else if (response.status >= 500 && response.status <= 600) {
                let errors = Object.assign({}, this.state.errors);
                errors.added = true;
                this.setState({errors});
                console.log(response.statusText);
            } else {
                console.log(response.statusText);
            }


        });
    }

    showAllBooks() {
        return (
            <select value={this.state.bookId} name="book" id="book" onChange={this.handleChangeBook}
                    className={AuthorForm.contactFormErrorClass(this.state.errors.bookId)}>

                <option value="0" disabled="disabled">-- vyberte knihu --</option>
                {this.state.books.map(book => (
                    <option key={book.id} value={book.id}>
                        {book.name}
                    </option>
                ))}

            </select>
        );

    }

    showAllLibraries() {
        return (
            <select value={this.state.libraryId} name="library" id="library" onChange={this.handleChangeLibrary}
                    className={AuthorForm.contactFormErrorClass(this.state.errors.libraryId)}>

                <option value="0" disabled="disabled">-- vyberte knihovnu --</option>
                {this.state.libraries.map(library => (
                    <option key={library.id} value={library.id}>
                        {library.name}
                    </option>
                ))}

            </select>
        );

    }

    render() {
        if (this.state.fireRedirect) {
            return (
                <Redirect to={'/bookAddedToLibrary'}/>
            );
        }
        return (

            <section>
                <h1>Přidání knihy do knihovny</h1>
                <form className="ContactForm putOut" onSubmit={this.handleSubmit}>
                    {this.showAllLibraries()}
                    {this.showAllBooks()}
                    <input className={'center'} type={'submit'} value={'Přidat knihu'}/>
                    <p className={'error' + ' ' + AuthorForm.labelErrorClass(this.state.errors.added)}>
                        Kniha nelze být přidána do knihovny.
                    </p>
                </form>

            </section>


        );
    }

}

export default BookToLibrary;






