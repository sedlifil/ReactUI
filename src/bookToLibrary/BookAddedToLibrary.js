import React, {Component} from 'react';
import {Link} from 'react-router-dom';


import '../css/main.css';

class BookAddedToLibrary extends Component {

    render() {
        return (
            <section>
                <h3>Kniha přidána do knihovny.</h3>
                <Link to="/bookToLibrary">přidat novou knihu</Link>
            </section>
        );
    }

}

export default BookAddedToLibrary;






