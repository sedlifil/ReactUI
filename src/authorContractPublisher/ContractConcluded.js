import React, {Component} from 'react';
import {Link} from 'react-router-dom';


import '../css/main.css';

class ContractConcluded extends Component {

    render() {
        return (
            <section>
                <h3>Smlouva uzavřena.</h3>
                <Link to="/authorsContractPublishers">uzavřít novou smlouvu</Link>
            </section>
        );
    }

}

export default ContractConcluded;






