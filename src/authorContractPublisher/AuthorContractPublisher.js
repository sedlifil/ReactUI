import React, {Component} from 'react';
import {Redirect} from 'react-router'

import '../css/main.css';
import '../css/form.css';
import AuthorForm from "../author/CreateAuthor";


class AuthorContractPublisher extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fireRedirect: false,
            authors: [],
            authorId: 0,
            publishers: [],
            publisherId: 0,
            errors: {
                authorId: false,
                publisherId: false,
                added: false,
                serverError: ""
            }
        };
        this.handleChangeAuthor = this.handleChangeAuthor.bind(this);
        this.handleChangePublisher = this.handleChangePublisher.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        fetch("http://localhost:8080/BookSystem/rest/authors")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        authors: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
        fetch("http://localhost:8080/BookSystem/rest/publishers")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        publishers: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );

    }

    handleChangeAuthor() {
        const authorId = document.getElementById("author").value;
        this.setState({authorId: authorId});
        let errors = Object.assign({}, this.state.errors);
        if (parseInt(authorId) !== 0) {
            errors.authorId = false;
            this.setState({errors});
            return true;
        }
        errors.authorId = true;
        this.setState({errors});
        return false;
    }

    handleChangePublisher() {
        const publisherId = document.getElementById("publisher").value;
        this.setState({publisherId: publisherId});
        let errors = Object.assign({}, this.state.errors);
        if (parseInt(publisherId) !== 0) {
            errors.publisherId = false;
            this.setState({errors});
            return true;
        }
        errors.publisherId = true;
        this.setState({errors});
        return false;
    }


    handleSubmit(e) {
        e.preventDefault();
        if (!this.handleChangePublisher()) {
            return;
        }
        if (!this.handleChangeAuthor()) {
            return;
        }

        const json = {"idAuthor": this.state.authorId, "idPublisher": this.state.publisherId};

        fetch('http://localhost:8080/BookSystem/rest/authorsContractPublishers', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(json)
        }).then(function (response) {
            if (!response) {
                throw Error(response.statusText);
            }
            return response;
        }).then((response) => {
            if (response.status === 200) {
                this.setState({fireRedirect: true});
                console.log("ok");
            } else if (response.status === 512) {
                let errors = Object.assign({}, this.state.errors);
                errors.added = true;
                errors.serverError = 'Autor nebo nakladatelství je zadáno nesprávně.';
                this.setState({errors});
                console.log(response.statusText);
            } else if (response.status === 513) {
                let errors = Object.assign({}, this.state.errors);
                errors.added = true;
                errors.serverError = 'Autor už má uzavřenou smlouvu s nakladatelstvím.';
                this.setState({errors});
                console.log(response.statusText);
            } else if (response.status === 514) {
                let errors = Object.assign({}, this.state.errors);
                errors.added = true;
                errors.serverError = 'Error s uzavřením smloucy.';
                this.setState({errors});
                console.log(response.statusText);
            } else {
                console.log(response.statusText);
            }


        });
    }

    showAllAuthors() {
        return (
            <select value={this.state.authorId} name="author" id="author" onChange={this.handleChangeAuthor}
                    className={AuthorForm.contactFormErrorClass(this.state.errors.authorId)}>

                <option value="0" disabled="disabled">-- vyberte autora --</option>
                {this.state.authors.map(author => (
                    <option key={author.id} value={author.id}>
                        {author.name} {author.surname}, {author.dateOfBirth}
                    </option>
                ))}

            </select>
        );

    }

    showAllPublishers() {
        return (
            <select value={this.state.publisherId} name="publisher" id="publisher" onChange={this.handleChangePublisher}
                    className={AuthorForm.contactFormErrorClass(this.state.errors.publisherId)}>

                <option value="0" disabled="disabled">-- vyberte nakladatelství --</option>
                {this.state.publishers.map(publisher => (
                    <option key={publisher.id} value={publisher.id}>
                        {publisher.name}
                    </option>
                ))}

            </select>
        );

    }

    render() {
        if (this.state.fireRedirect) {
            return (
                <Redirect to={'/contractConcluded'}/>
            );
        }
        return (

            <section>
                <h1>Podepsat smlouvu autora s nakladatelstvím</h1>
                <form className="ContactForm contract" onSubmit={this.handleSubmit}>
                    {this.showAllPublishers()}
                    {this.showAllAuthors()}
                    <input className={'center'} type={'submit'} value={'Přidat smlouvu'}/>
                    <p className={'error' + ' ' + AuthorForm.labelErrorClass(this.state.errors.added)}>
                        {this.state.errors.serverError}
                    </p>
                </form>
            </section>


        );
    }

}

export default AuthorContractPublisher;






