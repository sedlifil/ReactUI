import React, {Component} from 'react';
import {Link} from 'react-router-dom';


import '../css/main.css';


class Publisher extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: props.match.params.id,
            error: null,
            isLoaded: false,
            publisher: {}
        };
    }


    componentDidMount() {
        fetch("http://localhost:8080/BookSystem/rest/publishers/" + this.state.id)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        publisher: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    static showAllAuthors(publisher) {
        if (publisher.authors.toString() !== "") {
            return (
                <ul>
                    {publisher.authors.map(author => (
                        <li key={author.id}>
                            <Link to={`/authors/${author.id}`}>{author.name} {author.surname}</Link>

                        </li>

                    ))}
                </ul>);
        } else {
            return (
                <ul>
                    <li>žádná uzavřená smlouva</li>
                </ul>
            );
        }
    }

    static showAllBooks(publisher) {
        if (publisher.books.toString() !== "") {
            return (
                <ul>
                    {publisher.books.map(book => (
                        <li key={book.id}>
                            <Link to={`/books/${book.id}`}>{book.name}</Link>
                        </li>

                    ))}
                </ul>);
        } else {
            return (
                <ul>
                    <li>žádná vydaná kniha</li>
                </ul>
            );
        }
    }

    goBack = () => {
        this.props.history.goBack();
    };

    render() {
        const {id, error, isLoaded, publisher} = this.state;

        if (error) {
            return <div>{id} Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else if (publisher) {
            return (
                <section>
                    <h1>{publisher.name}</h1>
                    <table>


                        <tr>
                            <th>
                                Adresa:
                            </th>
                            <td>
                                {publisher.address}
                            </td>
                        </tr>

                        <tr className=''>
                            Smlouvy s autory:<br/>
                            {Publisher.showAllAuthors(publisher)}
                        </tr>

                        <tr className=''>
                            Vydané knihy:<br/>
                            {Publisher.showAllBooks(publisher)}
                        </tr>

                        <tr>
                            <button onClick={this.goBack.bind(this)}>Zpátky</button>
                        </tr>
                    </table>


                </section>
            );
        }
    }
}

export default Publisher;