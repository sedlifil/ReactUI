import React, {Component} from 'react';
import {Switch, Route} from 'react-router-dom'
import Publisher from "./Publisher";
import Publishers from "./Publishers";

import '../css/main.css';

class PublisherRouter extends Component {

    render() {
        return (
            <Switch>
                <Route exact path='/publishers' component={Publishers}/>
                <Route path='/publishers/:id' component={Publisher}/>
            </Switch>
        );
    }
}

export default PublisherRouter;