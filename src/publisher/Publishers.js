import React, {Component} from 'react';
import {Link} from 'react-router-dom';


import '../css/main.css';


class Publishers extends Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            data: []
        };
    }


    componentDidMount() {
        fetch("http://localhost:8080/BookSystemBlock1/rest/publishers")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        data: result
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    showAllAuthors() {
        const {data} = this.state;
        return (
            <section>
                <h1>Nakladatelství</h1>
                <table className='cyanTemplate'>
                    <thead>
                    <tr>
                        <th>
                            Název
                        </th>
                        <th>
                            Adresa
                        </th>

                    </tr>
                    </thead>
                    <tbody>
                    {data.map(publisher => (
                        <tr
                            key={publisher.id}>
                            <td>
                                <Link to={`/publishers/${publisher.id}`}>{publisher.name}</Link>
                            </td>
                            <td>
                                {publisher.address}
                            </td>

                        </tr>
                    ))}
                    </tbody>
                </table>
            </section>
        );
    }


    render() {
        const {error, isLoaded, data} = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else if (data) {
            return this.showAllAuthors();
        }
    }
}

export default Publishers;