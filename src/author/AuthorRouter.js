import React, {Component} from 'react';
import {Switch, Route} from 'react-router-dom'
import Author from "./Author";
import Authors from "./Authors";

import '../css/main.css';

class AuthorRouter extends Component {

    render() {
        return (
            <Switch>
                <Route exact path='/authors' component={Authors}/>
                <Route path='/authors/:id' component={Author}/>
            </Switch>
        );
    }
}

export default AuthorRouter;