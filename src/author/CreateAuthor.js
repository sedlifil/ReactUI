import React, {Component} from 'react';
import {Redirect} from 'react-router'
import moment from 'moment';

import '../css/main.css';
import '../css/form.css';


class AuthorForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fireRedirect: false,
            name: "",
            surname: "",
            email: "",
            dateOfBirth: "",
            errors: {
                name: false,
                surname: false,
                email: false,
                dateOfBirth: false,
                added: false,
                serverError: ""
            }
        };
        this.handleName = this.handleName.bind(this);
        this.handleEmail = this.handleEmail.bind(this);
        this.handleEmailBlur = this.handleEmailBlur.bind(this);
        this.handleSurname = this.handleSurname.bind(this);
        this.handleDateOfBirth = this.handleDateOfBirth.bind(this);
        this.handleDateOfBirthBlur = this.handleDateOfBirthBlur.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleName() {

        const nameAuthor = document.getElementById('name').value;
        this.setState({name: nameAuthor});
        let errors = Object.assign({}, this.state.errors);    //creating copy of object
        if (nameAuthor.length <= 0) {
            errors.name = true;
            this.setState({errors});
            return false;
        }
        errors.name = false;
        this.setState({errors});
        return true;
    }

    handleSurname() {
        const surname = document.getElementById('surname').value;
        this.setState({surname: surname});
        let errors = Object.assign({}, this.state.errors);    //creating copy of object
        if (surname.length <= 0) {
            errors.surname = true;
            this.setState({errors});
            return false;
        }
        errors.surname = false;
        this.setState({errors});
        return true;
    }

    handleEmail() {
        const email = document.getElementById('email').value;
        this.setState({email: email});
        if (this.state.errors.email) {
            this.handleEmailBlur();
        }
    }

    handleEmailBlur() {
        const email = document.getElementById('email').value;
        let errors = Object.assign({}, this.state.errors);    //creating copy of object
        if (!/.+@.+\..+/.test(email)) {
            errors.email = true;
            this.setState({errors});
            return false;
        }
        errors.email = false;
        this.setState({errors});
        return true;
    }

    handleDateOfBirth() {
        const dateOfBirth = document.getElementById('dateOfBirth').value;
        this.setState({dateOfBirth: dateOfBirth});
        if (this.state.errors.dateOfBirth) {
            this.handleDateOfBirthBlur();
        }
    }

    handleDateOfBirthBlur() {
        const dateOfBirth = document.getElementById('dateOfBirth').value;
        let errors = Object.assign({}, this.state.errors);    //creating copy of object
        if (moment(dateOfBirth.replace(/-/g, '.'), 'D.M.YYYY', true).isValid()) {
            errors.dateOfBirth = false;
            this.setState({errors});
            return true;
        }
        errors.dateOfBirth = true;
        this.setState({errors});
        return false;

    }


    handleSubmit(e) {
        e.preventDefault();
        if (!this.handleName()) {
            return;
        }
        if (!this.handleSurname()) {
            return;
        }
        if (!this.handleEmailBlur()) {
            return;
        }
        if (!this.handleDateOfBirthBlur()) {
            return;
        }

        const author = {
            "name": this.state.name,
            "surname": this.state.surname,
            "email": this.state.email,
            "dateOfBirth": this.state.dateOfBirth
        };
        //alert(JSON.stringify(author));
        fetch('http://localhost:8080/BookSystem/rest/authors', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(author)
        }).then(function (response) {
            if (!response) {
                throw Error(response.statusText);
            }
            return response;
        }).then((response) => {
            if (response.status === 200) {
                this.setState({fireRedirect: true});
                console.log("ok");
            }else if (response.status === 512) {
                let errors = Object.assign({}, this.state.errors);
                errors.added = true;
                errors.serverError = 'Error při vytváření autora.';
                this.setState({errors});
                console.log(response.statusText);
            }  else {
                console.log(response.statusText);
            }


        });
    }

    static contactFormErrorClass(state) {
        return state ? 'ContactForm-error' : '';
    }

    static labelErrorClass(state) {
        return state ? 'showError' : '';
    }


    render() {
        if (this.state.fireRedirect) {
            return (
                <Redirect to={'/authors'}/>
            );
        }
        return (

            <section>
                <h1>Vytvoř autora</h1>
                <form className="ContactForm" onSubmit={this.handleSubmit}>
                    <label htmlFor={'name'}>Jméno:</label>
                    <input id={'name'} className={AuthorForm.contactFormErrorClass(this.state.errors.name)}
                           type={'text'} value={this.state.name} placeholder={'jméno'} onChange={this.handleName}/>
                    <label className={'error' + ' ' + AuthorForm.labelErrorClass(this.state.errors.name)}>Prosím vyplňte
                        jméno.</label>

                    <label htmlFor={'surname'}>Příjmení:</label>
                    <input id={'surname'} className={AuthorForm.contactFormErrorClass(this.state.errors.surname)}
                           type={'text'} value={this.state.surname} placeholder={'příjmení'}
                           onChange={this.handleSurname}/>
                    <label className={'error' + ' ' + AuthorForm.labelErrorClass(this.state.errors.surname)}>Prosím
                        vyplňte příjmení.</label>

                    <label htmlFor={'email'}>E-mail:</label>
                    <input id={'email'} className={AuthorForm.contactFormErrorClass(this.state.errors.email)}
                           type={'text'} value={this.state.email}
                           placeholder={'e-mail'} onChange={this.handleEmail} onBlur={this.handleEmailBlur}/>
                    <label className={'error' + ' ' + AuthorForm.labelErrorClass(this.state.errors.email)}>Prosím
                        vyplňte správně email.</label>

                    <label htmlFor={'dateOfBirth'}>Datum narození:</label>
                    <input id={'dateOfBirth'}
                           className={AuthorForm.contactFormErrorClass(this.state.errors.dateOfBirth)} type={'text'}
                           value={this.state.dateOfBirth}
                           placeholder={'datum narození'} onChange={this.handleDateOfBirth}
                           onBlur={this.handleDateOfBirthBlur}/>
                    <label className={'error' + ' ' + AuthorForm.labelErrorClass(this.state.errors.dateOfBirth)}>Prosím
                        vyplňte správně datum narození.</label>

                    <input type={'submit'} value={'Vytvoř autora'}/>
                    <p className={'error' + ' ' + AuthorForm.labelErrorClass(this.state.errors.added)}>
                        {this.state.errors.serverError}
                    </p>
                </form>
            </section>


        );
    }

}

export default AuthorForm;







