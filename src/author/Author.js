import React, {Component} from 'react';
import {Link} from 'react-router-dom';


import '../css/main.css';


class Author extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: props.match.params.id,
            error: null,
            isLoaded: false,
            author: {}
        };
    }


    componentDidMount() {
        fetch("http://localhost:8080/BookSystem/rest/authors/" + this.state.id)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        author: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    static showAllPublishersList(author) {
        if (author.publishers.toString() !== "") {
            return (
                <ul>
                    {author.publishers.map(pub => (
                        <li key={pub.id}>
                            <Link to={`/publishers/${pub.id}`}>{pub.name}</Link>
                        </li>

                    ))}
                </ul>);
        } else {
            return (
                <ul>
                    <li>žádná uzavřená smlouva</li>
                </ul>
            );
        }
    }

    static showAllBooksList(author) {
        if (author.books.toString() !== "") {
            return (
                <ul>
                    {author.books.map(book => (
                        <li key={book.id}>
                            <Link to={`/books/${book.id}`}>{book.name}</Link>

                        </li>

                    ))}
                </ul>);
        } else {
            return (
                <ul>
                    <li>žádná napsaná kniha</li>
                </ul>
            );
        }
    }

    goBack = () => {
        this.props.history.goBack();
    };

    render() {
        const {id, error, isLoaded, author} = this.state;

        if (error) {
            return <section>{id} Error: {error.message}</section>;
        } else if (!isLoaded) {
            return <section>Loading...</section>;
        } else if (author) {
            return (
                <section>
                    <h1>{author.name} {author.surname}</h1>
                    <table>
                        <tr>
                            <th>
                                Datum narození:
                            </th>
                            <td>
                                {author.dateOfBirth}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Email:
                            </th>
                            <td>
                                {author.email.toLowerCase()}
                            </td>
                        </tr>
                        <tr className=''>
                            Smlouvy s nakladatelstvím:<br/>
                            {Author.showAllPublishersList(author)}
                        </tr>

                        <tr className=''>
                            Napsané knihy:<br/>
                            {Author.showAllBooksList(author)}
                        </tr>

                        <tr>
                            <button onClick={this.goBack.bind(this)}>Zpátky</button>
                        </tr>
                    </table>


                </section>
            );
        }
    }
}

export default Author;