import React, {Component} from 'react';
import {Redirect} from 'react-router'

import '../css/main.css';
import '../css/form.css';
import AuthorForm from "../author/CreateAuthor";


class PublisherPutOutBook extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fireRedirect: false,
            books: [],
            bookId: 0,
            publishers: [],
            publisherId: 0,
            errors: {
                bookId: false,
                publisherId: false,
                contract: false
            }
        };
        this.handleChangeBook = this.handleChangeBook.bind(this);
        this.handleChangePublisher = this.handleChangePublisher.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        fetch("http://localhost:8080/BookSystem/rest/books")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        books: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
        fetch("http://localhost:8080/BookSystem/rest/publishers")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        publishers: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );

    }

    handleChangeBook() {
        var bookId = document.getElementById("book").value;
        this.setState({bookId: bookId});
        let errors = Object.assign({}, this.state.errors);
        if (parseInt(bookId) !== 0) {
            errors.bookId = false;
            this.setState({errors});
            return true;
        }
        errors.bookId = true;
        this.setState({errors});
        return false;
    }

    handleChangePublisher() {
        var publisherId = document.getElementById("publisher").value;
        this.setState({publisherId: publisherId});
        let errors = Object.assign({}, this.state.errors);
        if (parseInt(publisherId) !== 0) {
            errors.publisherId = false;
            this.setState({errors});
            return true;
        }
        errors.publisherId = true;
        this.setState({errors});
        return false;
    }


    handleSubmit(e) {
        e.preventDefault();
        if (!this.handleChangePublisher()) {
            return;
        }
        if (!this.handleChangeBook()) {
            return;
        }

        var json = {"idBook": this.state.bookId, "idPublisher": this.state.publisherId};

        fetch('http://localhost:8080/BookSystem/rest/publisherBookRegister', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(json)
        }).then(function (response) {
            if (!response) {
                throw Error(response.statusText);
            }
            return response;
        }).then((response) => {
            if (response.status === 200) {
                this.setState({fireRedirect: true});
                console.log("ok");
            } else if (response.status === 512) {
                let errors = Object.assign({}, this.state.errors);
                errors.contract = true;
                this.setState({errors});
                console.log(response.statusText);
            }


        });
    }

    contactFormErrorClass(state) {
        return state ? 'ContactForm-error' : '';
    }

    showAllBooks() {
        return (
            <select value={this.state.bookId} name="book" id="book" onChange={this.handleChangeBook}
                    className={this.contactFormErrorClass(this.state.errors.bookId)}>

                <option value="0" disabled="disabled">-- vyberte knihu --</option>
                {this.state.books.map(book => (
                    <option key={book.id} value={book.id}>
                        {book.name}
                    </option>
                ))}

            </select>
        );

    }

    showAllPublishers() {
        return (
            <select value={this.state.publisherId} name="publisher" id="publisher" onChange={this.handleChangePublisher}
                    className={this.contactFormErrorClass(this.state.errors.publisherId)}>

                <option value="0" disabled="disabled">-- vyberte nakladatelství --</option>
                {this.state.publishers.map(publisher => (
                    <option key={publisher.id} value={publisher.id}>
                        {publisher.name}
                    </option>
                ))}

            </select>
        );

    }


    render() {
        if (this.state.fireRedirect) {
            return (
                <Redirect to={'/bookWasPutOut'}/>
            );
        }
        return (

            <section>
                <h1>Vydání nové knihy</h1>
                <form className="ContactForm putOut" onSubmit={this.handleSubmit}>
                    {this.showAllPublishers()}
                    {this.showAllBooks()}
                    <input className={'center'} type={'submit'} value={'Vydat knihu'}/>
                    <p className={'error' + ' ' + AuthorForm.labelErrorClass(this.state.errors.contract)}>
                        Všichni autoři nemají uzavřenou smlouvu s nakladatelstvím.
                    </p>
                </form>

            </section>


        );
    }

}

export default PublisherPutOutBook;






