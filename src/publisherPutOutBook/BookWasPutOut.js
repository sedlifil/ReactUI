import React, {Component} from 'react';
import {Link} from 'react-router-dom';


import '../css/main.css';

class BookWasPutOut extends Component {

    render() {
        return (
            <section>
                <h3>Kniha byla vydána.</h3>
                <Link to="/publisherBookRegister">vydat novou knihu</Link>
            </section>
        );
    }

}

export default BookWasPutOut;






