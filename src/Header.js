import React, {Component} from 'react';
import {Link} from 'react-router-dom';

import plus from './css/gfx/plus-gray.png';
import './css/menu.css';
import './css/main.css';


class Header extends Component {


    render() {
        return (
            <header>
                <div className="introduction">
                    Semestrální práce - Filip Sedliský
                </div>
                <nav>
                    <ul>
                        <li><Link to='/'>Domů</Link></li>
                        <li><a href={""}>Vytvoř</a>
                            <img className="plusMenu" src={plus} alt="plus"/>
                            <ul>
                                <li><Link to='/author'>Vytvoř autora</Link></li>
                                <li><Link to='/library'>Vytvoř knihovnu</Link></li>
                                <li><Link to='/publisher'>Vytvoř nakladatelství</Link></li>
                                <li><Link to='/book'>Vytvoř knihu</Link></li>
                            </ul>
                        </li>
                        <li><a href={""}>Ukaž</a>
                            <img className="plusMenu" src={plus} alt="plus"/>
                            <ul>
                                <li><Link to='/authors'>Ukaž autory</Link></li>
                                <li><Link to='/libraries'>Ukaž knihovny</Link></li>
                                <li><Link to='/publishers'>Ukaž nakladatelství</Link></li>
                                <li><Link to='/books'>Ukaž knihy</Link></li>
                            </ul>
                        </li>
                        <li><Link to='/authorsContractPublishers'>Podepsat smlouvu autor - nakladatelství</Link></li>
                        <li><Link to='/publisherBookRegister'>Vydat novou knihu</Link></li>
                        <li><Link to='/bookToLibrary'>Přidat knihu do knihovny</Link></li>
                    </ul>
                </nav>
            </header>
        );
    }
}

export default Header;
